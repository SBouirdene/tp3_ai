import numpy as np
import random


def load_iris_dataset(train_ratio):
    """Cette fonction a pour but de lire le dataset Iris

    Args:
        train_ratio: le ratio des exemples qui vont etre attribués à l'entrainement,
        le reste des exemples va etre utilisé pour les tests.
        Par exemple : si le ratio est 50%, il y aura 50% des exemple (75 exemples) qui vont etre utilisés
        pour l'entrainement, et 50% (75 exemples) pour le test.

    Retours:
        Cette fonction doit retourner 4 matrices de type Numpy: train, train_labels, test, et test_labels
		
        - train : une matrice numpy qui contient les exemples qui vont etre utilisés pour l'entrainement, chaque 
        ligne dans cette matrice représente un exemple d'entrainement.
		
        - train_labels : contient les labels (ou les étiquettes) pour chaque exemple dans train, de telle sorte
          que : train_labels[i] est l'etiquette pour l'exemple train[i]
        
        - test : une matrice numpy qui contient les exemples qui vont etre utilisés pour le test, chaque 
        ligne dans cette matrice représente un exemple de test.
		
        - test_labels : contient les étiquettes pour chaque exemple dans test, de telle sorte
          que : test_labels[i] est l'etiquette pour l'exemple test[i]
    """
    
    np.random.seed(1) # Pour avoir les meme nombres aléatoires à chaque initialisation.
    
    # Vous pouvez utiliser des valeurs numériques pour les différents types de classes, tel que :
    conversion_labels = {'Iris-setosa': 0, 'Iris-versicolor' : 1, 'Iris-virginica' : 2}
    
    # Le fichier du dataset est dans le dossier datasets en attaché 
    with open ('datasets/bezdekIris.data', 'r') as f:
        Iris_data = []
        for line in f:
            line = line.replace("\n","")
            splitted_line = line.split(",")
            for index_element in range(len(splitted_line)):
                if is_float(splitted_line[index_element]):
                    splitted_line[index_element] = float(splitted_line[index_element])
            Iris_data.append(splitted_line)

    Iris_data = np.array(Iris_data, dtype=object)
    Iris_matrix = np.asmatrix(Iris_data, dtype=object)
    np.random.shuffle(Iris_matrix)
    index_separation = int(len(Iris_matrix)*train_ratio)
    train = Iris_matrix[:index_separation, :-1]
    train_labels = Iris_matrix[:index_separation, -1]
    test = Iris_matrix[index_separation:,:-1]
    test_labels = Iris_matrix[index_separation:, -1]





    
    # TODO : le code ici pour lire le dataset
    
    # REMARQUE très importante : 
	# remarquez bien comment les exemples sont ordonnés dans 
    # le fichier du dataset, ils sont ordonnés par type de fleur, cela veut dire que 
    # si vous lisez les exemples dans cet ordre et que si par exemple votre ration est de 60%,
    # vous n'allez avoir aucun exemple du type Iris-virginica pour l'entrainement, pensez
    # donc à utiliser la fonction random.shuffle pour melanger les exemples du dataset avant de séparer
    # en train et test.
       
    
    # Tres important : la fonction doit retourner 4 matrices (ou vecteurs) de type Numpy. 
    return (train, train_labels, test, test_labels)
	
	
	
def load_wine_dataset(train_ratio):
    """Cette fonction a pour but de lire le dataset Binary Wine quality

    Args:
        train_ratio: le ratio des exemples (ou instances) qui vont servir pour l'entrainement,
        le rest des exemples va etre utilisé pour les test.

    Retours:
        Cette fonction doit retourner 4 matrices de type Numpy: train, train_labels, test, et test_labels
		
        - train : une matrice numpy qui contient les exemples qui vont etre utilisés pour l'entrainement, chaque 
        ligne dans cette matrice représente un exemple d'entrainement.
		
        - train_labels : contient les labels (ou les étiquettes) pour chaque exemple dans train, de telle sorte
          que : train_labels[i] est l'etiquette pour l'exemple train[i]
        
        - test : une matrice numpy qui contient les exemples qui vont etre utilisés pour le test, chaque 
        ligne dans cette matrice représente un exemple de test.
		
        - test_labels : contient les étiquettes pour chaque exemple dans test, de telle sorte
          que : test_labels[i] est l'etiquette pour l'exemple test[i]
    """
    
    np.random.seed(1) # Pour avoir les meme nombres aléatoires à chaque initialisation.

    # Le fichier du dataset est dans le dossier datasets en attaché 
    with open ('datasets/binary-winequality-white.csv', 'r') as f:
        Wine_data = []
        for line in f:
            line = line.replace("\n","")
            splitted_line = line.split(",")
            for index_element in range(len(splitted_line)):
                if is_float(splitted_line[index_element]):
                    splitted_line[index_element] = float(splitted_line[index_element])
            Wine_data.append(splitted_line)

    Wine_data = np.array(Wine_data, dtype=object)
    Wine_matrix = np.asmatrix(Wine_data)
    np.random.shuffle(Wine_matrix)
    index_separation = int(len(Wine_matrix)*train_ratio)
    train = Wine_matrix[:index_separation,:-1]
    train_labels = Wine_matrix[:index_separation,-1]
    test = Wine_matrix[index_separation:,:-1]
    test_labels = Wine_matrix[index_separation:,-1]
	
    # TODO : le code ici pour lire le dataset
    
	
	# La fonction doit retourner 4 structures de données de type Numpy.
    return (train, train_labels, test, test_labels)

def load_abalone_dataset(train_ratio):
    """
    Cette fonction a pour but de lire le dataset Abalone-intervalles

    Args:
        train_ratio: le ratio des exemples (ou instances) qui vont servir pour l'entrainement,
        le rest des exemples va etre utilisé pour les test.

    Retours:
        Cette fonction doit retourner 4 matrices de type Numpy: train, train_labels, test, et test_labels

        - train : une matrice numpy qui contient les exemples qui vont etre utilisés pour l'entrainement, chaque 
        ligne dans cette matrice représente un exemple d'entrainement.

        - train_labels : contient les labels (ou les étiquettes) pour chaque exemple dans train, de telle sorte
          que : train_labels[i] est l'etiquette pour l'exemple train[i]

        - test : une matrice numpy qui contient les exemples qui vont etre utilisés pour le test, chaque 
        ligne dans cette matrice représente un exemple de test.

        - test_labels : contient les étiquettes pour chaque exemple dans test, de telle sorte
          que : test_labels[i] est l'etiquette pour l'exemple test[i]
    """
    np.random.seed(1) # Pour avoir les meme nombres aléatoires à chaque initialisation.
    with open ('datasets/abalone-intervalles.csv', 'r') as f:
        Abalone_data = []
        for line in f:
            line = line.replace("\n","")
            splitted_line = line.split(",")
            for index_element in range(len(splitted_line)):
                if is_float(splitted_line[index_element]):
                    splitted_line[index_element] = float(splitted_line[index_element])
            Abalone_data.append(splitted_line)

    Abalone_data = np.array(Abalone_data, dtype=object)
    Abalone_matrix = np.asmatrix(Abalone_data)
    np.random.shuffle(Abalone_matrix)
    index_separation = int(len(Abalone_matrix)*train_ratio)
    train = Abalone_matrix[:-1, :index_separation]
    train_labels = Abalone_matrix[-1, :index_separation]
    test = Abalone_matrix[:-1, index_separation:]
    test_labels = Abalone_matrix[-1, index_separation:]
    return (train, train_labels, test, test_labels)


def is_float(value):
  try:
    float(value)
    return True
  except:
    return False
