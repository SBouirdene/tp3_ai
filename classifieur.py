"""
Vous allez definir une classe pour chaque algorithme que vous allez développer,
votre classe doit contenit au moins les 3 méthodes definies ici bas, 
	* train 	: pour entrainer le modèle sur l'ensemble d'entrainement.
	* predict 	: pour prédire la classe d'un exemple donné.
	* evaluate 		: pour evaluer le classifieur avec les métriques demandées. 
vous pouvez rajouter d'autres méthodes qui peuvent vous etre utiles, mais la correction
se fera en utilisant les méthodes train, predict et evaluate de votre code.
"""

import numpy as np


# le nom de votre classe
# BayesNaif pour le modèle bayesien naif
# Knn pour le modèle des k plus proches voisins

class Classifier: #nom de la class à changer

	def __init__(self, **kwargs):
		"""
		C'est un Initializer. 
		Vous pouvez passer d'autre paramètres au besoin,
		c'est à vous d'utiliser vos propres notations
		"""
		
		
	def train(self, train, train_labels): #vous pouvez rajouter d'autres attributs au besoin
		"""
		C'est la méthode qui va entrainer votre modèle,
		train est une matrice de type Numpy et de taille nxm, avec 
		n : le nombre d'exemple d'entrainement dans le dataset
		m : le mobre d'attribus (le nombre de caractéristiques)
		
		train_labels : est une matrice numpy de taille nx1
		
		vous pouvez rajouter d'autres arguments, il suffit juste de
		les expliquer en commentaire
		
		"""
        
	def predict(self, x):
		"""
		Prédire la classe d'un exemple x donné en entrée
		exemple est de taille 1xm
		"""
        
	def evaluate(self, X, y):
		"""
		c'est la méthode qui va evaluer votre modèle sur les données X
		l'argument X est une matrice de type Numpy et de taille nxm, avec 
		n : le nombre d'exemple de test dans le dataset
		m : le mobre d'attribus (le nombre de caractéristiques)
		
		y : est une matrice numpy de taille nx1
		
		vous pouvez rajouter d'autres arguments, il suffit juste de
		les expliquer en commentaire
		"""
        
	
	# Vous pouvez rajouter d'autres méthodes et fonctions,
	# il suffit juste de les commenter.




class KNN: #nom de la class à changer

	def __init__(self, train, train_labels, test, test_labels, K):
		"""
		C'est un Initializer. 
		Vous pouvez passer d'autre paramètres au besoin,
		c'est à vous d'utiliser vos propres notations
		"""
		self.train = train
		self.train_labels = train_labels
		self.test = test 
		self.test_labels = test_labels
		self.K = K 
		
		
	def train(self, train, train_labels, test, K): #vous pouvez rajouter d'autres attributs au besoin
		"""
		C'est la méthode qui va entrainer votre modèle,
		train est une matrice de type Numpy et de taille nxm, avec 
		n : le nombre d'exemple d'entrainement dans le dataset
		m : le mobre d'attribus (le nombre de caractéristiques)
		
		train_labels : est une matrice numpy de taille nx1
		
		vous pouvez rajouter d'autres arguments, il suffit juste de
		les expliquer en commentaire
		
		"""

		#AJOUTER MIN-MAX SCALING (pour avoir 0-1)
		list_labels_predicted = []
		for index_test in range(data_test):
			element_test = data_test[index_test]
			nearest_neighboors = [9999999] * K
			for index_train in data_train:
				element_train = data_train[index_train]
				element_score = 0
				index = 0
				while element_score < nearest_neighboors[K-1][1]:
					for feature in range(len(element_train)):
					if isinstance(element_train[feature], int):
						element_score += (element_train[feature] - element_test[feature])^2 
					else:
						if data_train[index] != data_test[index] # pour categoriel
							element_score += 1
				if element_score < nearest_neighboor[K-1][-1]:
					nearest_neighboors[K-1] = [element_train[0], element_score, train_labels[index_train]]
					nearest_neighboors.sort(key=lambda element: element[1])
			prediction = self.predict(nearest_neighboors)
			list_labels_predicted.append(prediction)
		return list_labels_predicted

        
	def predict(self, nearest_neighboors):
		"""
		Prédire la classe d'un exemple x donné en entrée
		exemple est de taille 1xm
		"""

		dic_label = {}
		for neighboor in nearest_neighboors:
			if neighboor[2] in dic_label.keys():
				dic_label[neighboor[2]] += 1
			else:
				dic_label[neighboor[2]]  = 1 
		label = max(dic_label, key=dic_label.get)
		return label



        
	def evaluate(self, test_labels, list_labels_predicted):
		"""
		c'est la méthode qui va evaluer votre modèle sur les données X
		l'argument X est une matrice de type Numpy et de taille nxm, avec 
		n : le nombre d'exemple de test dans le dataset
		m : le mobre d'attribus (le nombre de caractéristiques)
		
		y : est une matrice numpy de taille nx1
		
		vous pouvez rajouter d'autres arguments, il suffit juste de
		les expliquer en commentaire
		"""
		Dic_matrix = {}
		labels = np.unique(test_labels)
		for label in labels:
			Dic_matrix[label]["True"] = 0
			Dic_matrix[label]["False"] = 0 
		for index in range(test_labels):
			if list_labels_predicted[index] == test_labels[index]:
				Dic_matrix[list_labels_predicted[index]]["True"] += 1
			else:
				Dic_matrix[list_labels_predicted[index]]["False"] += 1
		if len(labels) > 2:
			accu_all = 0
			precision_all = 0
			recall_all = 0 

			for label in range(labels):
				label_count = np.count_nonzero(test_labels == label)
				label_tot = len(test_labels)
				TP = Dic_matrix[label]["True"]
				FP = Dic_matrix[label]["False"]
				TN = label_count - Dic_matrix[label]["True"]
				FN = label_tot - TP - FP - NP
				print("       | {} |   not {} |".format(label, label))
				print("Pred{} | {} | {} |".format(label, TP, FP))
				print("Pred{} | {} | {} |".format(label, FN, TN))

				print("")
				print("")

				accu_label = (TP + TN)/(TP + TN + FP + FN)
				accu_all += (accu_label * label_count / label_tot)
				print("accuracy of {} : {}".format(label,accu_label))

				precision_label = (TP )/(TP + FP )
				precision_all += (precision_label * label_count / label_tot)
				print("precision of {} : {}".format(label,precision_label))

				recall_label = (TP )/(TP + FN)
				recall_all += (recall_label * label_count / label_tot)
				print("recall of {} : {}".format(label,recall_label))

				F1_label = (2*precision_label*recall_label)/(precision_label + recall_label)
				print("F1 of {} : {}".format(label,F1_label))

			print("")
			print("")
			print("")
			print("")

			F1_all = (2*precision_all*recall_all)/(precision_all + recall_all)
			print("accuracy of all data : {}".format(accu_all))
			print("precision of all data : {}".format(precision_all))
			print("recall of all data : {}".format(recall_all))
			print("F1 of all data : {}".format(F1_all))

			return (accu_all)


		else:
				TP = Dic_matrix[labels[0]]["True"]
				FP = Dic_matrix[labels[0]]["False"]
				TN = Dic_matrix[labels[1]]["False"]
				FN = Dic_matrix[labels[1]]["True"]

				print("       | {} | {} |".format(labels[0], labels[1]))
				print("Pred{} | {} | {} |".format(labels[0], TP, FP))
				print("Pred{} | {} | {} |".format(labels[1], FN, TN))

				print("")
				print("")

				accu = (TP + TN)/(TP + TN + FP + FN)
				print("accuracy of {} : {}".format(label,accu))

				precision = (TP )/(TP + FP )
				print("precision of {} : {}".format(label,precision))

				recall = (TP)/(TP + FN)
				print("recall of {} : {}".format(label,recall))

				F1 = (2*precision*recall)/(precision + recall)
				print("F1 of {} : {}".format(label,F1))

				return (accu)

    
  #   def if_in_dic(self, dic, key):
  #   	if key in dic_label.keys():
		# 	dic[key] += 1
		# else:
		# 	dic[key]  = 1 
		# return dic 
	def Run_all(self):
		list_labels_predicted = self.train()
		accuracy = self.evaluate(self.train, self.train_labels, self.test, self.K)
		return accuracy



	
	# Vous pouvez rajouter d'autres méthodes et fonctions,
	# il suffit juste de les commenter.