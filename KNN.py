"""
Vous allez definir une classe pour chaque algorithme que vous allez développer,
votre classe doit contenit au moins les 3 méthodes definies ici bas, 
	* train 	: pour entrainer le modèle sur l'ensemble d'entrainement.
	* predict 	: pour prédire la classe d'un exemple donné.
	* evaluate 		: pour evaluer le classifieur avec les métriques demandées. 
vous pouvez rajouter d'autres méthodes qui peuvent vous etre utiles, mais la correction
se fera en utilisant les méthodes train, predict et evaluate de votre code.
"""

import numpy as np

class KNN: #nom de la class à changer

	def __init__(self, train, train_labels, test, test_labels, K):
		"""
		C'est un Initializer. 
		Vous pouvez passer d'autre paramètres au besoin,
		c'est à vous d'utiliser vos propres notations
		"""
		self.train_data = train
		self.train_labels = train_labels
		self.test_data = test 
		self.test_labels = test_labels
		self.K = K 
		
		
	def train(self, train, train_labels, test, K): #vous pouvez rajouter d'autres attributs au besoin
		"""
		C'est la méthode qui va entrainer votre modèle,
		train est une matrice de type Numpy et de taille nxm, avec 
		n : le nombre d'exemple d'entrainement dans le dataset
		m : le mobre d'attribus (le nombre de caractéristiques)
		
		train_labels : est une matrice numpy de taille nx1
		
		vous pouvez rajouter d'autres arguments, il suffit juste de
		les expliquer en commentaire
		
		"""

		#AJOUTER MIN-MAX SCALING (pour avoir 0-1)
		list_labels_predicted = []
		rows_test, col_tests = test.shape
		rows_train, col_train = train.shape

		for index_test in range(rows_test):
			nearest_neighboors = []
			index = 0


			for index_train in range(rows_train):
				element_score = 0




				if index < K: 
					for feature in range(col_train):

							if isinstance(train[index_train,feature], float) or isinstance(train[index_train,feature], int):
								element_score += abs(train[index_train,feature] - test[index_test,feature]) 

							else:
								if train[index_train,feature] != test[index_test,feature]: # pour categoriel
									element_score += 1
					
					nearest_neighboors.append([element_score, train_labels[index_train,0]])
					nearest_neighboors.sort(key=lambda element: element[0])






				else:
					for feature in range(col_train):

						if isinstance(train[index_train,feature], int):
							element_score += abs(train[index_train,feature] - test[index_test,feature]) 

						else:
							if train[index_train,feature] != test[index_test,feature]: # pour categoriel
								element_score += 1

					if element_score < nearest_neighboors[K-1][0]:
						nearest_neighboors[K-1] = [element_score, train_labels[index_train,0]]
						nearest_neighboors.sort(key=lambda element: element[0])



				index +=1 
			prediction = self.predict(nearest_neighboors)
			list_labels_predicted.append(prediction)
		return list_labels_predicted

        



	def predict(self, nearest_neighboors):
		"""
		Prédire la classe d'un exemple x donné en entrée
		exemple est de taille 1xm
		"""

		dic_label = {}
		for neighboor in nearest_neighboors:
			if neighboor[1] in dic_label.keys():
				dic_label[neighboor[1]] += 1
			else:
				dic_label[neighboor[1]]  = 1 
		label = max(dic_label.items(), key = lambda k : k[1])
		return label[0]



        
	def evaluate(self, test_labels, list_labels_predicted):
		"""
		c'est la méthode qui va evaluer votre modèle sur les données X
		l'argument X est une matrice de type Numpy et de taille nxm, avec 
		n : le nombre d'exemple de test dans le dataset
		m : le mobre d'attribus (le nombre de caractéristiques)
		
		y : est une matrice numpy de taille nx1
		
		vous pouvez rajouter d'autres arguments, il suffit juste de
		les expliquer en commentaire
		"""

		Dic_matrix = {}
		dic_labels = {}
		rows_test, col_tests = test_labels.shape
		for i in range(rows_test):
			if test_labels[i,0] not in dic_labels.keys():
				dic_labels[test_labels[i,0]] = 1
			else:
				dic_labels[test_labels[i,0]] += 1
		labels = dic_labels.keys()
		print(labels)

		for label in labels:
			Dic_matrix[label] = {}
			Dic_matrix[label]["True"] = 0
			Dic_matrix[label]["False"] = 0 
		for index in range(len(test_labels)):
			if list_labels_predicted[index] == test_labels[index, 0]:
				Dic_matrix[list_labels_predicted[index]]["True"] += 1
			else:
				Dic_matrix[list_labels_predicted[index]]["False"] += 1
		if len(labels) > 2:
			accu_all = 0
			precision_all = 0
			recall_all = 0 
			label_tot = len(test_labels)
			for label in labels:
				label_count = dic_labels[label]
				print(label_count) 

				TP = Dic_matrix[label]["True"]
				FP = Dic_matrix[label]["False"]
				FN = label_count - Dic_matrix[label]["True"]
				TN = label_tot - TP - FP - FN
				print("       | {} |   not {} |".format(label, label))
				print("Pred{} | {} | {} |".format(label, TP, FP))
				print("Pred{} | {} | {} |".format(label, FN, TN))

				print("")
				print("")

				accu_label = (TP + TN)/(TP + TN + FP + FN)

				if TP == 0 :
					precision_label = 0
					recall_label = 0
					F1_label = 0
				else:
					precision_label = (TP )/(TP + FP )
					recall_label = (TP )/(TP + FN)
					F1_label = (2*precision_label*recall_label)/(precision_label + recall_label)

				print("accuracy of {} : {}".format(label,accu_label))
				print("precision of {} : {}".format(label,precision_label))
				print("recall of {} : {}".format(label,recall_label))
				print("F1 of {} : {}".format(label,F1_label))

			print("")
			print("")
			print("")
			print("")
			accu_all += (accu_label * label_count / label_tot)
			precision_all += (precision_label * label_count / label_tot)
			recall_all += (recall_label * label_count / label_tot)
			F1_all = (2*precision_all*recall_all)/(precision_all + recall_all)
			print("accuracy of all data : {}".format(accu_all))
			print("precision of all data : {}".format(precision_all))
			print("recall of all data : {}".format(recall_all))
			print("F1 of all data : {}".format(F1_all))

			return (accu_all)


		else:
				TP = Dic_matrix[labels[0]]["True"]
				FP = Dic_matrix[labels[0]]["False"]
				TN = Dic_matrix[labels[1]]["False"]
				FN = Dic_matrix[labels[1]]["True"]


				print("       | {} | {} |".format(labels[0], labels[1]))
				print("Pred{} | {} | {} |".format(labels[0], TP, FP))
				print("Pred{} | {} | {} |".format(labels[1], FN, TN))

				print("")
				print("")

				accu = (TP + TN)/(TP + TN + FP + FN)
				print("accuracy of {} : {}".format(label,accu))

				precision = (TP )/(TP + FP )
				print("precision of {} : {}".format(label,precision))

				recall = (TP)/(TP + FN)
				print("recall of {} : {}".format(label,recall))

				F1 = (2*precision*recall)/(precision + recall)
				print("F1 of {} : {}".format(label,F1))

				return (accu)

	def MinMax_scaling(self):
		rows_test, col_tests = self.test_data.shape
		rows_train, col_train = self.train_data.shape
		for i in range(col_train):
			if isinstance(self.train_data[0,i], float):
				max_train = max(self.train_data[:,i])
				max_test = max(self.test_data[:,i])
				max_data = max([max_train,max_test])
				min_train = min(self.train_data[:,i])
				min_test = min(self.test_data[:,i])
				min_data = min([min_train,min_test])
		for row_train in range(rows_train):
			for column_train in range(col_train):
				self.train_data[row_train,column_train] = float((self.train_data[row_train,column_train] - min_data)/(max_data + min_data))
		for row_test in range(rows_test):
			for column_test in range(col_tests):
				self.test_data[row_test,column_test] = (self.test_data[row_test,column_test] - min_data)/(max_data + min_data)








    

	def Run_all(self):
		self.MinMax_scaling()
		list_labels_predicted = self.train(self.train_data , self.train_labels, self.test_data , self.K)
		accuracy = self.evaluate(self.test_labels, list_labels_predicted)
		return accuracy



	
	# Vous pouvez rajouter d'autres méthodes et fonctions,
	# il suffit juste de les commenter.