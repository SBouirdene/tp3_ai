import numpy as np
import sys
import load_datasets
# import NaiveBayes # importer la classe du classifieur bayesien
import KNN # importer la classe du Knn
#importer d'autres fichiers et classes si vous en avez développés
import copy
import os 
import sys
"""
C'est le fichier main duquel nous allons tout lancer
Vous allez dire en commentaire c'est quoi les paramètres que vous avez utilisés
En gros, vous allez :
1- Initialiser votre classifieur avec ses paramètres
2- Charger les datasets
3- Entrainer votre classifieur
4- Le tester

"""

# Initialisez vos paramètres


K_list = [3,4,5,6,7,8,9,10,11,12,13,14,15]


# Initialisez/instanciez vos classifieurs avec leurs paramètres





# Charger/lire les datasets

ratio = 0.8
iris_train, iris_train_labels, iris_test, iris_test_labels = load_datasets.load_iris_dataset(ratio)
wine_train, wine_train_labels, wine_test, wine_test_labels = load_datasets.load_wine_dataset(ratio)
abalone_train, abalone_train_labels, abalone_test, abalone_test_labels = load_datasets.load_abalone_dataset(ratio)


class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout

def cross_validation(data, label, name_classifier, K = None, n=10):
    chunck_size = int(len(data)/n)
    sum_accu = 0
    sum_recall = 0
    sum_preci = 0
    list_accu = []
    begining_sep = 0
    std = 0
    for data_chunk_index in range(n):
        data_copy = copy.deepcopy(data)
        label_copy = copy.deepcopy(label)
        data_test = data_copy[begining_sep : begining_sep + chunck_size,:]
        label_test = label_copy[begining_sep : begining_sep + chunck_size,:]
        list_delet= list(range(begining_sep , begining_sep + chunck_size))
        data_train =  np.delete(data_copy, list_delet,0)
        label_train = np.delete(label_copy,list_delet, 0)
        begining_sep += chunck_size
        if name_classifier == "KNN":
            # with HiddenPrints():
            knn = KNN.KNN(data_train, label_train, data_test,label_test, K)
            accu = knn.Run_all()
            list_accu.append(accu)
            sum_accu += accu
        else:
            accu = classifier(data_train, label_train, data_test, label_test)
            list_accu.append(accu)
            sum_accu += accu
    mean_accu = sum_accu/n 
    for accu in list_accu:
        std += pow((mean_accu - accu),2)
    std = pow(std/n, 1/2)
    print("accuracy of {} (K={}) = {} +/- {}".format(name_classifier,K,mean_accu,std))


for k in K_list:
    cross_validation(iris_train, iris_train_labels, "KNN", k, 4)


# neigh = KNeighborsClassifier(n_neighbors=5)
# neigh.fit(X_train, labels_train)
# k_pred =  neigh.predict(X_test)

# print("KNN")
# print(confusion_matrix(labels_test, k_pred))
# print('Accuracy : {0:.2f}%, Précision : {1:.2f}%'.format(100*accuracy_score(labels_test,k_pred),100*precision_score(labels_test,k_pred))) 

# KNN = KNN.KNN(iris_train, iris_train_labels, iris_test, iris_test_labels, K_list[2])
# KNN.Run_all()

# Entrainez votre classifieur


"""
Après avoir fait l'entrainement, évaluez votre modèle sur 
les données d'entrainement.
IMPORTANT : 
    Vous devez afficher ici avec la commande print() de python,
    - la matrice de confusion (confusion matrix)
    - l'accuracy
    - la précision (precision)
    - le rappel (recall)
    - le F1-score
"""




# Tester votre classifieur



"""
Finalement, évaluez votre modèle sur les données de test.
IMPORTANT : 
    Vous devez afficher ici avec la commande print() de python,
    - la matrice de confusion (confusion matrix)
    - l'accuracy
    - la précision (precision)
    - le rappel (recall)
    - le F1-score
"""






